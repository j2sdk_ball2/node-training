const Koa = require('koa')
const app = new Koa()
const router = require('koa-router')()
const validator = require('koa-validate')
const bodyParser = require('koa-bodyparser')
const port = 3050

module.exports = start

function start() {

    validator(app)
    app.use(bodyParser())

    router.get('/', async (context, next) => {
        context.status = 200
        context.body = {
            message: 'Get / OK'
        }
        next()
    })

    router.post('/', async (context, next) => {
        context.status = 200
        context.body = {
            message: 'Post / OK'
        }
        next()
    })

    router.post('/api/v1/login', 
        async (context, next) => {
            context.checkBody('email')
                .notEmpty('empty')
                .isEmail("invalid format")

            context.checkBody('password')
                .notEmpty('empty')
                .len(8, 50, {
                    min: 8,
                    max: 50
                }).md5()

            if (context.errors) {
                context.status = 400
                return context.body = context.errors
            }
            next()
        },
        async (context, next) => {
            context.status = 200
            context.body = {
                message: 'Login successfully'
            }
            next()
        }
    )

    app.use(router.routes())
    app.use(router.allowedMethods())

    const listener = app.listen(port)
    console.log(`server is running on port ${port}`)
}
